# looks like -*- vcl -*-

varnishtest "fileread()"

shell {printf "0123456789abcdef" > ${tmpdir}/f.txt}
shell {rm -f ${tmpdir}/nonexistent.txt}
shell {mkfifo ${tmpdir}/fifo.txt}

varnish v1 -vcl { backend b { .host = "${bad_ip}"; } } -start

varnish v1 -errvcl {vmod gcrypt error: libgcrypt initialization not finished in gcrypt.fileread()} {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new aes = gcrypt.symmetric(AES, ECB, NONE,
		              key=gcrypt.fileread("${tmpdir}/nonexistent.txt"));
	}
}

varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(FINISH);
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.f1 = blob.encode(IDENTITY, DEFAULT,
		                            gcrypt.fileread("${tmpdir}/f.txt"));
		set resp.http.f2 = blob.encode(IDENTITY, DEFAULT,
		                            gcrypt.fileread("${tmpdir}/f.txt"));
		set resp.http.n = blob.encode(IDENTITY, DEFAULT,
		                  gcrypt.fileread("${tmpdir}/nonexistent.txt"));
		set resp.http.fifo = blob.encode(IDENTITY, DEFAULT,
		                         gcrypt.fileread("${tmpdir}/fifo.txt"));

		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.f1 == "0123456789abcdef"
	expect resp.http.f2 == "0123456789abcdef"
	expect resp.http.n == ""
	expect resp.http.fifo == ""
} -run

logexpect l1 -v v1 -d 1 -q "VCL_Error" {
	expect 0 * Begin req
	expect * = VCL_Error "^vmod gcrypt error: Cannot stat .+nonexistent.txt in gcrypt.fileread..: .+$"
	expect * = VCL_Error "^vmod gcrypt error: .+fifo.txt is not a regular file in gcrypt.fileread..$"
	expect * = End
} -run

# The intended use case
varnish v1 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new aes = gcrypt.symmetric(AES, ECB, NONE,
		                        key=gcrypt.fileread("${tmpdir}/f.txt"));
	}
}

varnish v1 -errvcl {vmod gcrypt error: Cannot stat} {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new aes = gcrypt.symmetric(AES, ECB, NONE,
		              key=gcrypt.fileread("${tmpdir}/nonexistent.txt"));
	}
}

varnish v1 -errvcl {is not a regular file} {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new aes = gcrypt.symmetric(AES, ECB, NONE,
		                     key=gcrypt.fileread("${tmpdir}/fifo.txt"));
	}
}
