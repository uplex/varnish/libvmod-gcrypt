# looks like -*- vcl -*-

varnishtest "init function usage"

varnish v1 -errvcl {Not available in subroutine 'vcl_recv'} {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_recv {
		gcrypt.init(FINISH);
		return(synth(200));
	}
}

# Finish default initialization
varnish v1 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(FINISH);
	}
} -start

# Calling init again without restarting Varnish, even after reloading
# VCL, has no effect except to log the fact that initialization is
# already finished.
varnish v1 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(FINISH);
	}
}

logexpect l1 -v v1 -d 1 -g raw -q "Debug" {
	expect * 0 Debug "^libgcrypt initialization already finished$"
} -run

# The same is true even if we discard all VCL instances that used the
# VMOD.
varnish v1 -vcl { backend b { .host = "${bad_ip}"; } }
varnish v1 -cli "vcl.discard vcl1"
varnish v1 -cli "vcl.discard vcl2"

varnish v1 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(INIT_SECMEM, 32KB);
		gcrypt.init(FINISH);
	}
}

logexpect l1 -v v1 -d 1 -g raw -q "Debug" {
	expect * 0 Debug "^libgcrypt initialization already finished$"
	expect * = Debug "^libgcrypt initialization already finished$"
} -run

# So now we will have to start new Varnish instances for further
# tests.
varnish v1 -stop

# Objects cannot be constructed unless initialization is finished.
varnish v2 -vcl {backend b { .host = "${bad_ip}"; } } -start
varnish v2 -errvcl {libgcrypt initialization not finished in aes constructor} {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	import blob;
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k = blob.blob(HEX, "00000000000000000000000000000000");
		new aes = gcrypt.symmetric(AES, ECB, key=k.get());
	}
}

varnish v2 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	import blob;
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(FINISH);
		new k = blob.blob(HEX, "00000000000000000000000000000000");
		new aes = gcrypt.symmetric(AES, ECB, key=k.get());
	}
}

# After initialization but before a restart, a new VCL can construct
# object instances without having to call init(FINISH) again.
varnish v2 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	import blob;
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k = blob.blob(HEX, "00000000000000000000000000000000");
		new aes = gcrypt.symmetric(AES, ECB, key=k.get());
	}
}
