# looks like -*- vcl -*-

varnishtest "padding"

varnish v1 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(FINISH);
	}
} -start

# Test padding -- encrypt with padding, decrypt with no padding, and
# verify that the result has padding bytes as expected.
varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k1 = blob.blob(HEX, "000102030405060708090a0b0c0d0e0f");
		new none = gcrypt.symmetric(AES, ECB, NONE, key=k1.get());
		new pkcs7 = gcrypt.symmetric(AES, ECB, PKCS7, key=k1.get());
		new iso7816 = gcrypt.symmetric(AES, ECB, ISO7816, key=k1.get());
		new x923 = gcrypt.symmetric(AES, ECB, X923, key=k1.get());

		new foo = blob.blob(encoded="foo");
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.foo-pkcs7-cipher
		    = blob.encode(BASE64, DEFAULT, pkcs7.encrypt(foo.get()));
		set resp.http.foo-pkcs7-plain
		    = blob.encode(HEX, UPPER, none.decrypt(
		          blob.decode(BASE64, encoded=resp.http.foo-pkcs7-cipher)));
		set resp.http.foo-iso7816-cipher
		    = blob.encode(BASE64, DEFAULT, iso7816.encrypt(foo.get()));
		set resp.http.foo-iso7816-plain
		    = blob.encode(HEX, UPPER, none.decrypt(
		        blob.decode(BASE64, encoded=resp.http.foo-iso7816-cipher)));
		set resp.http.foo-x923-cipher
		    = blob.encode(BASE64, DEFAULT, x923.encrypt(foo.get()));
		set resp.http.foo-x923-plain
		    = blob.encode(HEX, UPPER, none.decrypt(
		        blob.decode(BASE64, encoded=resp.http.foo-x923-cipher)));

		set resp.http.empty-pkcs7-cipher
		    = blob.encode(BASE64, DEFAULT,
		                    pkcs7.encrypt(blob.decode(encoded="")));
		set resp.http.empty-pkcs7-plain
		    = blob.encode(HEX, UPPER, none.decrypt(
		        blob.decode(BASE64, encoded=resp.http.empty-pkcs7-cipher)));
		set resp.http.empty-iso7816-cipher
		    = blob.encode(BASE64, DEFAULT,
		                  iso7816.encrypt(blob.decode(encoded="")));
		set resp.http.empty-iso7816-plain
		    = blob.encode(HEX, UPPER, none.decrypt(
		      blob.decode(BASE64, encoded=resp.http.empty-iso7816-cipher)));
		set resp.http.empty-x923-cipher
		    = blob.encode(BASE64, DEFAULT,
		                     x923.encrypt(blob.decode(encoded="")));
		set resp.http.empty-x923-plain
		    = blob.encode(HEX, UPPER, none.decrypt(
		        blob.decode(BASE64, encoded=resp.http.empty-x923-cipher)));
		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.foo-pkcs7-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-pkcs7-plain == "666F6F0D0D0D0D0D0D0D0D0D0D0D0D0D"
	expect resp.http.foo-iso7816-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-iso7816-plain == "666F6F80000000000000000000000000"
	expect resp.http.foo-x923-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-x923-plain == "666F6F0000000000000000000000000D"
	expect resp.http.empty-pkcs7-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-pkcs7-plain == "10101010101010101010101010101010"
	expect resp.http.empty-iso7816-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-iso7816-plain == "80000000000000000000000000000000"
	expect resp.http.empty-x923-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-x923-plain == "00000000000000000000000000000010"
} -run

# Test unpadding -- encrypt a plaintext without padding, where the
# plaintext has the exact block length and padding bytes already
# added. Then decrypt with padding, and verify that the result has
# padding removed.
varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k1 = blob.blob(HEX, "000102030405060708090a0b0c0d0e0f");
		new none = gcrypt.symmetric(AES, ECB, NONE, key=k1.get());
		new pkcs7 = gcrypt.symmetric(AES, ECB, PKCS7, key=k1.get());
		new iso7816 = gcrypt.symmetric(AES, ECB, ISO7816, key=k1.get());
		new x923 = gcrypt.symmetric(AES, ECB, X923, key=k1.get());

		new foo_pkcs7
		    = blob.blob(HEX, "666F6F0D0D0D0D0D0D0D0D0D0D0D0D0D");
		new foo_iso7816
		    = blob.blob(HEX, "666F6F80000000000000000000000000");
		new foo_x923
		    = blob.blob(HEX, "666F6F0000000000000000000000000D");

		new empty_pkcs7
		    = blob.blob(HEX, "10101010101010101010101010101010");
		new empty_iso7816
		    = blob.blob(HEX, "80000000000000000000000000000000");
		new empty_x923
		    = blob.blob(HEX, "00000000000000000000000000000010");
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.foo-pkcs7-cipher
		    = blob.encode(BASE64, DEFAULT, none.encrypt(foo_pkcs7.get()));
		set resp.http.foo-pkcs7-plain
		    = blob.encode(blob=pkcs7.decrypt(
		          blob.decode(BASE64, encoded=resp.http.foo-pkcs7-cipher)));
		set resp.http.foo-iso7816-cipher
		    = blob.encode(BASE64, DEFAULT, none.encrypt(foo_iso7816.get()));
		set resp.http.foo-iso7816-plain
		    = blob.encode(blob=iso7816.decrypt(
		        blob.decode(BASE64, encoded=resp.http.foo-iso7816-cipher)));
		set resp.http.foo-x923-cipher
		    = blob.encode(BASE64, DEFAULT, none.encrypt(foo_x923.get()));
		set resp.http.foo-x923-plain
		    = blob.encode(blob=x923.decrypt(
		        blob.decode(BASE64, encoded=resp.http.foo-x923-cipher)));

		set resp.http.empty-pkcs7-cipher
		    = blob.encode(BASE64, DEFAULT, none.encrypt(empty_pkcs7.get()));
		set resp.http.empty-pkcs7-plain
		    = blob.encode(blob=pkcs7.decrypt(
		        blob.decode(BASE64, encoded=resp.http.empty-pkcs7-cipher)));
		set resp.http.empty-iso7816-cipher
		   = blob.encode(BASE64, DEFAULT, none.encrypt(empty_iso7816.get()));
		set resp.http.empty-iso7816-plain
		    = blob.encode(blob=iso7816.decrypt(
		      blob.decode(BASE64, encoded=resp.http.empty-iso7816-cipher)));
		set resp.http.empty-x923-cipher
		    = blob.encode(BASE64, DEFAULT, none.encrypt(empty_x923.get()));
		set resp.http.empty-x923-plain
		    = blob.encode(blob=x923.decrypt(
		        blob.decode(BASE64, encoded=resp.http.empty-x923-cipher)));
		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.foo-pkcs7-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-pkcs7-plain == "foo"
	expect resp.http.foo-iso7816-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-iso7816-plain == "foo"
	expect resp.http.foo-x923-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-x923-plain == "foo"
	expect resp.http.empty-pkcs7-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-pkcs7-plain == ""
	expect resp.http.empty-iso7816-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-iso7816-plain == ""
	expect resp.http.empty-x923-cipher ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-x923-plain == ""
} -run

