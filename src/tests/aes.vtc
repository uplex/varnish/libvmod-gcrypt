# looks like -*- vcl -*-

varnishtest "AES"

varnish v1 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(FINISH);
	}
} -start

# from selftest() in libgcrypt cipher/rijndael.c
varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k1 = blob.blob(HEX, "E8E9EAEBEDEEEFF0F2F3F4F5F7F8F9FA");
		new aes = gcrypt.symmetric(AES, ECB, NONE, key=k1.get());
		new p1 = blob.blob(HEX, "014BAF2278A69D331D5180103643E99A");
		new c1 = blob.blob(HEX, "6743C3D1519AB4F2CD9A78AB09A511BD");
		# The next 3 cipher ENUMs are just aliases for AES.
		new aes128 = gcrypt.symmetric(AES128, ECB, NONE, key=k1.get());
		new rijndael
		    = gcrypt.symmetric(RIJNDAEL, ECB, NONE, key=k1.get());
		new rijndael128
		    = gcrypt.symmetric(RIJNDAEL128, ECB, NONE, key=k1.get());
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.aes-ciphertext
		    = blob.encode(HEX, UPPER, aes.encrypt(p1.get()));
		set resp.http.aes-plaintext
		    = blob.encode(HEX, UPPER, aes.decrypt(c1.get()));
		set resp.http.aes128-ciphertext
		    = blob.encode(HEX, UPPER, aes128.encrypt(p1.get()));
		set resp.http.aes128-plaintext
		    = blob.encode(HEX, UPPER, aes128.decrypt(c1.get()));
		set resp.http.rijndael-ciphertext
		    = blob.encode(HEX, UPPER, rijndael.encrypt(p1.get()));
		set resp.http.rijndael-plaintext
		    = blob.encode(HEX, UPPER, rijndael.decrypt(c1.get()));
		set resp.http.rijndael128-ciphertext
		    = blob.encode(HEX, UPPER, rijndael128.encrypt(p1.get()));
		set resp.http.rijndael128-plaintext
		    = blob.encode(HEX, UPPER, rijndael128.decrypt(c1.get()));
		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.aes-ciphertext == "6743C3D1519AB4F2CD9A78AB09A511BD"
	expect resp.http.aes-plaintext == "014BAF2278A69D331D5180103643E99A"
	expect resp.http.aes128-ciphertext == resp.http.aes-ciphertext
	expect resp.http.aes128-plaintext == resp.http.aes-plaintext
	expect resp.http.rijndael-ciphertext == resp.http.aes-ciphertext
	expect resp.http.rijndael-plaintext == resp.http.aes-plaintext
	expect resp.http.rijndael128-ciphertext == resp.http.aes-ciphertext
	expect resp.http.rijndael128-plaintext == resp.http.aes-plaintext
} -run

varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k1 = blob.blob(HEX,
		    "04050607090A0B0C0E0F10111314151618191A1B1D1E1F20");
		new aes192 = gcrypt.symmetric(AES192, ECB, NONE, key=k1.get());
		new p1 = blob.blob(HEX, "76777475F1F2F3F4F8F9E6E777707172");
		new c1 = blob.blob(HEX, "5D1EF20DCED6BCBC12131AC7C54788AA");

		new k2 = blob.blob(HEX,
	    "08090A0B0D0E0F10121314151718191A1C1D1E1F21222324262728292B2C2D2E");
		new aes256 = gcrypt.symmetric(AES256, ECB, NONE, key=k2.get());
		new p2 = blob.blob(HEX, "069A007FC76A459F98BAF917FEDF9521");
		new c2 = blob.blob(HEX, "080E9517EB1677719ACF728086040AE3");

		# RIJNDAEL192 and -256 are aliases for AES192 and -256.
		new rijndael192
		    = gcrypt.symmetric(RIJNDAEL192, ECB, NONE, key=k1.get());
		new rijndael256
		    = gcrypt.symmetric(RIJNDAEL256, ECB, NONE, key=k2.get());
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.aes192-ciphertext
		    = blob.encode(HEX, UPPER, aes192.encrypt(p1.get()));
		set resp.http.aes192-plaintext
		    = blob.encode(HEX, UPPER, aes192.decrypt(c1.get()));
		set resp.http.rijndael192-ciphertext
		    = blob.encode(HEX, UPPER, rijndael192.encrypt(p1.get()));
		set resp.http.rijndael192-plaintext
		    = blob.encode(HEX, UPPER, rijndael192.decrypt(c1.get()));
		set resp.http.aes256-ciphertext
		    = blob.encode(HEX, UPPER, aes256.encrypt(p2.get()));
		set resp.http.aes256-plaintext
		    = blob.encode(HEX, UPPER, aes256.decrypt(c2.get()));
		set resp.http.rijndael256-ciphertext
		    = blob.encode(HEX, UPPER, rijndael256.encrypt(p2.get()));
		set resp.http.rijndael256-plaintext
		    = blob.encode(HEX, UPPER, rijndael256.decrypt(c2.get()));
		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.aes192-ciphertext == "5D1EF20DCED6BCBC12131AC7C54788AA"
	expect resp.http.aes192-plaintext == "76777475F1F2F3F4F8F9E6E777707172"
	expect resp.http.rijndael192-ciphertext == resp.http.aes192-ciphertext
	expect resp.http.rijndael192-plaintext == resp.http.aes192-plaintext
	expect resp.http.aes256-ciphertext == "080E9517EB1677719ACF728086040AE3"
	expect resp.http.aes256-plaintext == "069A007FC76A459F98BAF917FEDF9521"
	expect resp.http.rijndael256-ciphertext == resp.http.aes256-ciphertext
	expect resp.http.rijndael256-plaintext == resp.http.aes256-plaintext
} -run

# from seltest_fips_128_38a() in libgcrypt cipher/rijndael.c
varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k = blob.blob(HEX, "2b7e151628aed2a6abf7158809cf4f3c");
		new iv = blob.blob(HEX, "000102030405060708090a0b0c0d0e0f");

		new p1 = blob.blob(HEX, "6bc1bee22e409f96e93d7e117393172a");
		new p2 = blob.blob(HEX, "ae2d8a571e03ac9c9eb76fac45af8e51");
		new p3 = blob.blob(HEX, "30c81c46a35ce411e5fbc1191a0a52ef");
		new p4 = blob.blob(HEX, "f69f2445df4f9b17ad2b417be66c3710");
		new p = blob.blob(HEX,
"6bc1bee22e409f96e93d7e117393172aae2d8a571e03ac9c9eb76fac45af8e5130c81c46a35ce411e5fbc1191a0a52eff69f2445df4f9b17ad2b417be66c3710");

		new c1 = blob.blob(HEX, "3b3fd92eb72dad20333449f8e83cfb4a");

		new aes_cfb = gcrypt.symmetric(AES, CFB, key=k.get());
		new c_cfb2
		    = blob.blob(HEX, "c8a64537a0b3a93fcde3cdad9f1ce58b");
		new c_cfb3
		    = blob.blob(HEX, "26751f67a3cbb140b1808cf187a4f4df");
		new c_cfb4
		    = blob.blob(HEX, "c04b05357c5d1c0eeac4c66f9ff7f2e6");
		new c_cfb
		    = blob.blob(HEX,
"3b3fd92eb72dad20333449f8e83cfb4ac8a64537a0b3a93fcde3cdad9f1ce58b26751f67a3cbb140b1808cf187a4f4dfc04b05357c5d1c0eeac4c66f9ff7f2e6");

		new aes_ofb = gcrypt.symmetric(AES, OFB, key=k.get());
		new c_ofb2
		    = blob.blob(HEX, "7789508d16918f03f53c52dac54ed825");
		new c_ofb3
		    = blob.blob(HEX, "9740051e9c5fecf64344f7a82260edcc");
		new c_ofb4
		    = blob.blob(HEX, "304c6528f659c77866a510d9c1d6ae5e");
		new c_ofb
		    = blob.blob(HEX,
"3b3fd92eb72dad20333449f8e83cfb4a7789508d16918f03f53c52dac54ed8259740051e9c5fecf64344f7a82260edcc304c6528f659c77866a510d9c1d6ae5e");
		new out1
		    = blob.blob(HEX, "50fe67cc996d32b6da0937e99bafec60");
		new out2
		    = blob.blob(HEX, "d9a4dada0892239f6b8b3d7680e15674");
		new out3
		    = blob.blob(HEX, "a78819583f0308e7a6bf36b1386abf23");
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.cfb-ciphertext-1
		    = blob.encode(HEX, LOWER, aes_cfb.encrypt(p1.get(),
		                                             iv=iv.get()));
		set resp.http.cfb-plaintext-1
		    = blob.encode(HEX, LOWER, aes_cfb.decrypt(c1.get(),
		                                             iv=iv.get()));
		set resp.http.cfb-ciphertext-2
		    = blob.encode(HEX, LOWER, aes_cfb.encrypt(p2.get(),
		                                             iv=c1.get()));
		set resp.http.cfb-plaintext-2
		    = blob.encode(HEX, LOWER, aes_cfb.decrypt(c_cfb2.get(),
		                                             iv=c1.get()));
		set resp.http.cfb-ciphertext-3
		    = blob.encode(HEX, LOWER, aes_cfb.encrypt(p3.get(),
		                                             iv=c_cfb2.get()));
		set resp.http.cfb-plaintext-3
		    = blob.encode(HEX, LOWER, aes_cfb.decrypt(c_cfb3.get(),
		                                             iv=c_cfb2.get()));
		set resp.http.cfb-ciphertext-4
		    = blob.encode(HEX, LOWER, aes_cfb.encrypt(p4.get(),
		                                             iv=c_cfb3.get()));
		set resp.http.cfb-plaintext-4
		    = blob.encode(HEX, LOWER, aes_cfb.decrypt(c_cfb4.get(),
		                                             iv=c_cfb3.get()));
		set resp.http.cfb-ciphertext
		    = blob.encode(HEX, LOWER, aes_cfb.encrypt(p.get(),
		                                             iv=iv.get()));
		set resp.http.cfb-plaintext
		    = blob.encode(HEX, LOWER, aes_cfb.decrypt(c_cfb.get(),
		                                             iv=iv.get()));

		set resp.http.ofb-ciphertext-1
		    = blob.encode(HEX, LOWER, aes_ofb.encrypt(p1.get(),
		                                             iv=iv.get()));
		set resp.http.ofb-plaintext-1
		    = blob.encode(HEX, LOWER, aes_ofb.decrypt(c1.get(),
		                                             iv=iv.get()));
		set resp.http.ofb-ciphertext-2
		    = blob.encode(HEX, LOWER, aes_ofb.encrypt(p2.get(),
		                                             iv=out1.get()));
		set resp.http.ofb-plaintext-2
		    = blob.encode(HEX, LOWER, aes_ofb.decrypt(c_ofb2.get(),
		                                             iv=out1.get()));
		set resp.http.ofb-ciphertext-3
		    = blob.encode(HEX, LOWER, aes_ofb.encrypt(p3.get(),
		                                             iv=out2.get()));
		set resp.http.ofb-plaintext-3
		    = blob.encode(HEX, LOWER, aes_ofb.decrypt(c_ofb3.get(),
		                                             iv=out2.get()));
		set resp.http.ofb-ciphertext-4
		    = blob.encode(HEX, LOWER, aes_ofb.encrypt(p4.get(),
		                                             iv=out3.get()));
		set resp.http.ofb-plaintext-4
		    = blob.encode(HEX, LOWER, aes_ofb.decrypt(c_ofb4.get(),
		                                             iv=out3.get()));
		set resp.http.ofb-ciphertext
		    = blob.encode(HEX, LOWER, aes_ofb.encrypt(p.get(),
		                                             iv=iv.get()));
		set resp.http.ofb-plaintext
		    = blob.encode(HEX, LOWER, aes_ofb.decrypt(c_ofb.get(),
		                                             iv=iv.get()));
		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.cfb-ciphertext-1 == "3b3fd92eb72dad20333449f8e83cfb4a"
	expect resp.http.cfb-plaintext-1 == "6bc1bee22e409f96e93d7e117393172a"
	expect resp.http.cfb-ciphertext-2 == "c8a64537a0b3a93fcde3cdad9f1ce58b"
	expect resp.http.cfb-plaintext-2 == "ae2d8a571e03ac9c9eb76fac45af8e51"
	expect resp.http.cfb-ciphertext-3 == "26751f67a3cbb140b1808cf187a4f4df"
	expect resp.http.cfb-plaintext-3 == "30c81c46a35ce411e5fbc1191a0a52ef"
	expect resp.http.cfb-ciphertext-4 == "c04b05357c5d1c0eeac4c66f9ff7f2e6"
	expect resp.http.cfb-plaintext-4 == "f69f2445df4f9b17ad2b417be66c3710"
	expect resp.http.cfb-ciphertext-4 == "c04b05357c5d1c0eeac4c66f9ff7f2e6"
	expect resp.http.cfb-plaintext-4 == "f69f2445df4f9b17ad2b417be66c3710"
	expect resp.http.cfb-ciphertext == "3b3fd92eb72dad20333449f8e83cfb4ac8a64537a0b3a93fcde3cdad9f1ce58b26751f67a3cbb140b1808cf187a4f4dfc04b05357c5d1c0eeac4c66f9ff7f2e6"
	expect resp.http.cfb-plaintext == "6bc1bee22e409f96e93d7e117393172aae2d8a571e03ac9c9eb76fac45af8e5130c81c46a35ce411e5fbc1191a0a52eff69f2445df4f9b17ad2b417be66c3710"

	expect resp.http.ofb-ciphertext-1 == resp.http.cfb-ciphertext-1
	expect resp.http.ofb-plaintext-1 == resp.http.cfb-plaintext-1
	expect resp.http.ofb-ciphertext-2 == "7789508d16918f03f53c52dac54ed825"
	expect resp.http.ofb-plaintext-2 == resp.http.cfb-plaintext-2
	expect resp.http.ofb-ciphertext-3 == "9740051e9c5fecf64344f7a82260edcc"
	expect resp.http.ofb-plaintext-3 == resp.http.cfb-plaintext-3
	expect resp.http.ofb-ciphertext-4 == "304c6528f659c77866a510d9c1d6ae5e"
	expect resp.http.ofb-plaintext-4 == resp.http.cfb-plaintext-4
	expect resp.http.ofb-ciphertext-4 == "304c6528f659c77866a510d9c1d6ae5e"
	expect resp.http.ofb-plaintext-4 == resp.http.cfb-plaintext-4
	expect resp.http.ofb-ciphertext == "3b3fd92eb72dad20333449f8e83cfb4a7789508d16918f03f53c52dac54ed8259740051e9c5fecf64344f7a82260edcc304c6528f659c77866a510d9c1d6ae5e"
	expect resp.http.ofb-plaintext == resp.http.cfb-plaintext
} -run

# from check_aes128_cbc_cts_cipher() in libgcrypt tests/basic.c
varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k = blob.blob(encoded="chicken teriyaki");
		new iv = blob.blob(encoded="");
		new aes = gcrypt.symmetric(AES, CBC, key=k.get(), cbc_cts=true);
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.plaintext =
	"I would like the General Gau's Chicken, please, and wonton soup.";

		set resp.http.ciphertext17
		    = blob.encode(HEX, LOWER,
				      aes.encrypt(blob.decode(length=17,
						  encoded=resp.http.plaintext),
						  iv=iv.get()));
		set resp.http.ciphertext31
		    = blob.encode(HEX, LOWER,
				      aes.encrypt(blob.decode(length=31,
						  encoded=resp.http.plaintext),
						  iv=iv.get()));
		set resp.http.ciphertext32
		    = blob.encode(HEX, LOWER,
				      aes.encrypt(blob.decode(length=32,
						  encoded=resp.http.plaintext),
						  iv=iv.get()));
		set resp.http.ciphertext47
		    = blob.encode(HEX, LOWER,
				      aes.encrypt(blob.decode(length=47,
						  encoded=resp.http.plaintext),
						  iv=iv.get()));
		set resp.http.ciphertext48
		    = blob.encode(HEX, LOWER,
				      aes.encrypt(blob.decode(length=48,
						  encoded=resp.http.plaintext),
						  iv=iv.get()));
		set resp.http.ciphertext64
		    = blob.encode(HEX, LOWER,
				      aes.encrypt(blob.decode(length=64,
						  encoded=resp.http.plaintext),
						  iv=iv.get()));
		set resp.http.ciphertext
		    = blob.encode(HEX, LOWER,
				      aes.encrypt(blob.decode(
						  encoded=resp.http.plaintext),
						    iv=iv.get()));
		set resp.http.plaintext17
		    = blob.encode(blob=aes.decrypt(blob.decode(HEX,
						       encoded=resp.http.ciphertext17),
						       iv=iv.get()));
		set resp.http.plaintext31
		    = blob.encode(blob=aes.decrypt(blob.decode(HEX,
						       encoded=resp.http.ciphertext31),
						       iv=iv.get()));
		set resp.http.plaintext32
		    = blob.encode(blob=aes.decrypt(blob.decode(HEX,
						       encoded=resp.http.ciphertext32),
						       iv=iv.get()));
		set resp.http.plaintext47
		    = blob.encode(blob=aes.decrypt(blob.decode(HEX,
						       encoded=resp.http.ciphertext47),
						       iv=iv.get()));
		set resp.http.plaintext48
		    = blob.encode(blob=aes.decrypt(blob.decode(HEX,
						       encoded=resp.http.ciphertext48),
						       iv=iv.get()));
		set resp.http.plaintext64
		    = blob.encode(blob=aes.decrypt(blob.decode(HEX,
						       encoded=resp.http.ciphertext64),
						       iv=iv.get()));
		set resp.http.plaintext
		    = blob.encode(blob=aes.decrypt(blob.decode(HEX,
						       encoded=resp.http.ciphertext),
						       iv=iv.get()));

		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.ciphertext17 == "c6353568f2bf8cb4d8a580362da7ff7f97"
	expect resp.http.ciphertext31 == "fc00783e0efdb2c1d445d4c8eff7ed2297687268d6ecccc0c07b25e25ecfe5"
	expect resp.http.ciphertext32 == "39312523a78662d5be7fcbcc98ebf5a897687268d6ecccc0c07b25e25ecfe584"
	expect resp.http.ciphertext47 == "97687268d6ecccc0c07b25e25ecfe584b3fffd940c16a18c1b5549d2f838029e39312523a78662d5be7fcbcc98ebf5"
	expect resp.http.ciphertext48 == "97687268d6ecccc0c07b25e25ecfe5849dad8bbb96c4cdc03bc103e1a194bbd839312523a78662d5be7fcbcc98ebf5a8"
	expect resp.http.ciphertext64 == "97687268d6ecccc0c07b25e25ecfe58439312523a78662d5be7fcbcc98ebf5a84807efe836ee89a526730dbc2f7bc8409dad8bbb96c4cdc03bc103e1a194bbd8"
	expect resp.http.ciphertext == resp.http.ciphertext64
	expect resp.http.plaintext17 == "I would like the "
	expect resp.http.plaintext31 == "I would like the General Gau's "
	expect resp.http.plaintext32 == "I would like the General Gau's C"
	expect resp.http.plaintext47 == "I would like the General Gau's Chicken, please,"
	expect resp.http.plaintext48 == "I would like the General Gau's Chicken, please, "
	expect resp.http.plaintext64 == "I would like the General Gau's Chicken, please, and wonton soup."
	expect resp.http.plaintext == resp.http.plaintext64
} -run

# from check_ctr_cipher() in libgcrypt tests/basic.c
varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new ctr1 = blob.blob(HEX,
		                         "f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff");
		new ctr2 = blob.blob(HEX,
		                         "f0f1f2f3f4f5f6f7f8f9fafbfcfdff00");
		new ctr3 = blob.blob(HEX,
		                         "f0f1f2f3f4f5f6f7f8f9fafbfcfdff01");
		new ctr4 = blob.blob(HEX,
		                         "f0f1f2f3f4f5f6f7f8f9fafbfcfdff02");

		new p1 = blob.blob(HEX, "6bc1bee22e409f96e93d7e117393172a");
		new p2 = blob.blob(HEX, "ae2d8a571e03ac9c9eb76fac45af8e51");
		new p3 = blob.blob(HEX, "30c81c46a35ce411e5fbc1191a0a52ef");
		new p4 = blob.blob(HEX, "f69f2445df4f9b17ad2b417be66c3710");
		new p = blob.blob(HEX,
"6bc1bee22e409f96e93d7e117393172aae2d8a571e03ac9c9eb76fac45af8e5130c81c46a35ce411e5fbc1191a0a52eff69f2445df4f9b17ad2b417be66c3710");

		new k128 = blob.blob(HEX,
		                         "2b7e151628aed2a6abf7158809cf4f3c");
		new aes128 = gcrypt.symmetric(AES, CTR, key=k128.get());
		new c128_1 = blob.blob(HEX,
		                           "874d6191b620e3261bef6864990db6ce");
		new c128_2 = blob.blob(HEX,
		                           "9806f66b7970fdff8617187bb9fffdff");
		new c128_3 = blob.blob(HEX,
		                           "5ae4df3edbd5d35e5b4f09020db03eab");
		new c128_4 = blob.blob(HEX,
		                           "1e031dda2fbe03d1792170a0f3009cee");
		new c128 = blob.blob(HEX,
"874d6191b620e3261bef6864990db6ce9806f66b7970fdff8617187bb9fffdff5ae4df3edbd5d35e5b4f09020db03eab1e031dda2fbe03d1792170a0f3009cee");

		new k192 = blob.blob(HEX,
	                    "8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b");
		new aes192 = gcrypt.symmetric(AES192, CTR, key=k192.get());
		new c192_1 = blob.blob(HEX,
		                           "1abc932417521ca24f2b0459fe7e6e0b");
		new c192_2 = blob.blob(HEX,
		                           "090339ec0aa6faefd5ccc2c6f4ce8e94");
		new c192_3 = blob.blob(HEX,
		                           "1e36b26bd1ebc670d1bd1d665620abf7");
		new c192_4 = blob.blob(HEX,
		                           "4f78a7f6d29809585a97daec58c6b050");
		new c192 = blob.blob(HEX,
"1abc932417521ca24f2b0459fe7e6e0b090339ec0aa6faefd5ccc2c6f4ce8e941e36b26bd1ebc670d1bd1d665620abf74f78a7f6d29809585a97daec58c6b050");

		new k256 = blob.blob(HEX,
	    "603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4");
		new aes256 = gcrypt.symmetric(AES256, CTR, key=k256.get());
		new c256_1 = blob.blob(HEX,
		                           "601ec313775789a5b7a7f504bbf3d228");
		new c256_2 = blob.blob(HEX,
		                           "f443e3ca4d62b59aca84e990cacaf5c5");
		new c256_3 = blob.blob(HEX,
		                           "2b0930daa23de94ce87017ba2d84988d");
		new c256_4 = blob.blob(HEX,
		                           "dfc9c58db67aada613c2dd08457941a6");
		new c256 = blob.blob(HEX,
"601ec313775789a5b7a7f504bbf3d228f443e3ca4d62b59aca84e990cacaf5c52b0930daa23de94ce87017ba2d84988ddfc9c58db67aada613c2dd08457941a6");
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.c128-1 = blob.encode(HEX, LOWER,
		    aes128.encrypt(p1.get(), ctr=ctr1.get()));
		set resp.http.p128-1 = blob.encode(HEX, LOWER,
		    aes128.decrypt(c128_1.get(), ctr=ctr1.get()));
		set resp.http.c128-2 = blob.encode(HEX, LOWER,
		    aes128.encrypt(p2.get(), ctr=ctr2.get()));
		set resp.http.p128-2 = blob.encode(HEX, LOWER,
		    aes128.decrypt(c128_2.get(), ctr=ctr2.get()));
		set resp.http.c128-3 = blob.encode(HEX, LOWER,
		    aes128.encrypt(p3.get(), ctr=ctr3.get()));
		set resp.http.p128-3 = blob.encode(HEX, LOWER,
		    aes128.decrypt(c128_3.get(), ctr=ctr3.get()));
		set resp.http.c128-4 = blob.encode(HEX, LOWER,
		    aes128.encrypt(p4.get(), ctr=ctr4.get()));
		set resp.http.p128-4 = blob.encode(HEX, LOWER,
		    aes128.decrypt(c128_4.get(), ctr=ctr4.get()));
		set resp.http.c128 = blob.encode(HEX, LOWER,
		    aes128.encrypt(p.get(), ctr=ctr1.get()));
		set resp.http.p128 = blob.encode(HEX, LOWER,
		    aes128.decrypt(c128.get(), ctr=ctr1.get()));

		set resp.http.c192-1 = blob.encode(HEX, LOWER,
		    aes192.encrypt(p1.get(), ctr=ctr1.get()));
		set resp.http.p192-1 = blob.encode(HEX, LOWER,
		    aes192.decrypt(c192_1.get(), ctr=ctr1.get()));
		set resp.http.c192-2 = blob.encode(HEX, LOWER,
		    aes192.encrypt(p2.get(), ctr=ctr2.get()));
		set resp.http.p192-2 = blob.encode(HEX, LOWER,
		    aes192.decrypt(c192_2.get(), ctr=ctr2.get()));
		set resp.http.c192-3 = blob.encode(HEX, LOWER,
		    aes192.encrypt(p3.get(), ctr=ctr3.get()));
		set resp.http.p192-3 = blob.encode(HEX, LOWER,
		    aes192.decrypt(c192_3.get(), ctr=ctr3.get()));
		set resp.http.c192-4 = blob.encode(HEX, LOWER,
		    aes192.encrypt(p4.get(), ctr=ctr4.get()));
		set resp.http.p192-4 = blob.encode(HEX, LOWER,
		    aes192.decrypt(c192_4.get(), ctr=ctr4.get()));
		set resp.http.c192 = blob.encode(HEX, LOWER,
		    aes192.encrypt(p.get(), ctr=ctr1.get()));
		set resp.http.p192 = blob.encode(HEX, LOWER,
		    aes192.decrypt(c192.get(), ctr=ctr1.get()));

		set resp.http.c256-1 = blob.encode(HEX, LOWER,
		    aes256.encrypt(p1.get(), ctr=ctr1.get()));
		set resp.http.p256-1 = blob.encode(HEX, LOWER,
		    aes256.decrypt(c256_1.get(), ctr=ctr1.get()));
		set resp.http.c256-2 = blob.encode(HEX, LOWER,
		    aes256.encrypt(p2.get(), ctr=ctr2.get()));
		set resp.http.p256-2 = blob.encode(HEX, LOWER,
		    aes256.decrypt(c256_2.get(), ctr=ctr2.get()));
		set resp.http.c256-3 = blob.encode(HEX, LOWER,
		    aes256.encrypt(p3.get(), ctr=ctr3.get()));
		set resp.http.p256-3 = blob.encode(HEX, LOWER,
		    aes256.decrypt(c256_3.get(), ctr=ctr3.get()));
		set resp.http.c256-4 = blob.encode(HEX, LOWER,
		    aes256.encrypt(p4.get(), ctr=ctr4.get()));
		set resp.http.p256-4 = blob.encode(HEX, LOWER,
		    aes256.decrypt(c256_4.get(), ctr=ctr4.get()));
		set resp.http.c256 = blob.encode(HEX, LOWER,
		    aes256.encrypt(p.get(), ctr=ctr1.get()));
		set resp.http.p256 = blob.encode(HEX, LOWER,
		    aes256.decrypt(c256.get(), ctr=ctr1.get()));

		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200

	expect resp.http.c128-1 == "874d6191b620e3261bef6864990db6ce"
	expect resp.http.p128-1 == "6bc1bee22e409f96e93d7e117393172a"
	expect resp.http.c128-2 == "9806f66b7970fdff8617187bb9fffdff"
	expect resp.http.p128-2 == "ae2d8a571e03ac9c9eb76fac45af8e51"
	expect resp.http.c128-3 == "5ae4df3edbd5d35e5b4f09020db03eab"
	expect resp.http.p128-3 == "30c81c46a35ce411e5fbc1191a0a52ef"
	expect resp.http.c128-4 == "1e031dda2fbe03d1792170a0f3009cee"
	expect resp.http.p128-4 == "f69f2445df4f9b17ad2b417be66c3710"
	expect resp.http.c128 == "874d6191b620e3261bef6864990db6ce9806f66b7970fdff8617187bb9fffdff5ae4df3edbd5d35e5b4f09020db03eab1e031dda2fbe03d1792170a0f3009cee"
	expect resp.http.p128 == "6bc1bee22e409f96e93d7e117393172aae2d8a571e03ac9c9eb76fac45af8e5130c81c46a35ce411e5fbc1191a0a52eff69f2445df4f9b17ad2b417be66c3710"

	expect resp.http.c192-1 == "1abc932417521ca24f2b0459fe7e6e0b"
	expect resp.http.p192-1 == resp.http.p128-1
	expect resp.http.c192-2 == "090339ec0aa6faefd5ccc2c6f4ce8e94"
	expect resp.http.p192-2 == resp.http.p128-2
	expect resp.http.c192-3 == "1e36b26bd1ebc670d1bd1d665620abf7"
	expect resp.http.p192-3 == resp.http.p128-3
	expect resp.http.c192-4 == "4f78a7f6d29809585a97daec58c6b050"
	expect resp.http.p192-4 == resp.http.p128-4
	expect resp.http.c192 == "1abc932417521ca24f2b0459fe7e6e0b090339ec0aa6faefd5ccc2c6f4ce8e941e36b26bd1ebc670d1bd1d665620abf74f78a7f6d29809585a97daec58c6b050"
	expect resp.http.p192 == resp.http.p128

	expect resp.http.c256-1 == "601ec313775789a5b7a7f504bbf3d228"
	expect resp.http.p256-1 == resp.http.p128-1
	expect resp.http.c256-2 == "f443e3ca4d62b59aca84e990cacaf5c5"
	expect resp.http.p256-2 == resp.http.p128-2
	expect resp.http.c256-3 == "2b0930daa23de94ce87017ba2d84988d"
	expect resp.http.p256-3 == resp.http.p128-3
	expect resp.http.c256-4 == "dfc9c58db67aada613c2dd08457941a6"
	expect resp.http.p256-4 == resp.http.p128-4
	expect resp.http.c256 == "601ec313775789a5b7a7f504bbf3d228f443e3ca4d62b59aca84e990cacaf5c52b0930daa23de94ce87017ba2d84988ddfc9c58db67aada613c2dd08457941a6"
	expect resp.http.p256 == resp.http.p128
} -run

# from Google cryptogwt CipherTestVectors.java, test AES/CBC/PKCS7
varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new p6 = blob.blob(HEX, "f6cee5ff28fd");
		new k6 = blob.blob(HEX, "ac5800ac3cb59c7c14f36019e43b44fe");
		new i6 = blob.blob(HEX, "f013ce1ec901b5b60a85a986b3b72eba");
		new c6 = blob.blob(HEX, "e8a846fd9718507371604504d4ca1ac7");
		new aes6 = gcrypt.symmetric(AES, CBC, PKCS7, key=k6.get());

		new p7 = blob.blob(HEX, "76cdfdf52a9753");
		new k7 = blob.blob(HEX, "24c4328aeffc0ca354a3215a3da23a38");
		new i7 = blob.blob(HEX, "c43c6269bb8c1dbba3bc22b7ba7e24b1");
		new c7 = blob.blob(HEX, "009e935f3fe4d57b57fc3127a8873d8c");
		new aes7 = gcrypt.symmetric(AES, CBC, PKCS7, key=k7.get());

		new p8 = blob.blob(HEX, "b103c928531d8875");
		new k8 = blob.blob(HEX, "4035227440a779dbd1ed75c6ae78cef5");
		new i8 = blob.blob(HEX, "8faff161a5ec06e051066a571d1729d9");
		new c8 = blob.blob(HEX, "b3d8df2c3147b0752a7e6bbbcc9d5758");
		new aes8 = gcrypt.symmetric(AES, CBC, PKCS7, key=k8.get());

		new p9 = blob.blob(HEX, "590b10224087872724");
		new k9 = blob.blob(HEX, "507008732ea559915e5e45d9710e3ed2");
		new i9 = blob.blob(HEX, "342b22c1cbf1c92b8e63a38de99ffb09");
		new c9 = blob.blob(HEX, "c11a034ed324aeae9cd5857ae4cd776f");
		new aes9 = gcrypt.symmetric(AES, CBC, PKCS7, key=k9.get());

		new p10 = blob.blob(HEX, "ccecfa22708b6d06439c");
		new k10 =
		    blob.blob(HEX, "a060441b1b7cc2af405be4f6f5c58e22");
		new i10 =
		    blob.blob(HEX, "429d3240207e77e9b9dade05426fe3cb");
		new c10 =
		    blob.blob(HEX, "b61ff0a956b420347daa25bb76964b51");
		new aes10 = gcrypt.symmetric(AES, CBC, PKCS7, key=k10.get());

		new p11 = blob.blob(HEX, "8ff539940bae985f2f88f3");
		new k11 =
		    blob.blob(HEX, "721888e260b8925fe51183b88d65fb17");
		new i11 =
		    blob.blob(HEX, "5308c58068cbc05a5461a43bf744b61e");
		new c11 =
		    blob.blob(HEX, "3ee8bdb21b00e0103ccbf9afb9b5bd9a");
		new aes11 = gcrypt.symmetric(AES, CBC, PKCS7, key=k11.get());

		new p12 = blob.blob(HEX, "4c84974b5b2109d5bc90e1f0");
		new k12 =
		    blob.blob(HEX, "80ba985c93763f99ff4be6cdee6ab977");
		new i12 =
		    blob.blob(HEX, "ca8e99719be2e842e81bf15c606bb916");
		new c12 =
		    blob.blob(HEX, "3e087f92a998ad531e0ff8e996098382");
		new aes12 = gcrypt.symmetric(AES, CBC, PKCS7, key=k12.get());

		new p13 = blob.blob(HEX, "13eb26baf2b688574cadac6dba");
		new k13 =
		    blob.blob(HEX, "1fe107d14dd8b152580f3dea8591fc3b");
		new i13 =
		    blob.blob(HEX, "7b6070a896d41d227cc0cebbd92d797e");
		new c13 =
		    blob.blob(HEX, "a4bfd6586344bcdef94f09d871ca8a16");
		new aes13 = gcrypt.symmetric(AES, CBC, PKCS7, key=k13.get());

		new p14 = blob.blob(HEX, "5fcb46a197ddf80a40f94dc21531");
		new k14 =
		    blob.blob(HEX, "4d3dae5d9e19950f278b0dd4314e3768");
		new i14 =
		    blob.blob(HEX, "80190b58666f15dbaf892cf0bceb2a50");
		new c14 =
		    blob.blob(HEX, "2b166eae7a2edfea7a482e5f7377069e");
		new aes14 = gcrypt.symmetric(AES, CBC, PKCS7, key=k14.get());

		new p15 = blob.blob(HEX, "6842455a2992c2e5193056a5524075");
		new k15 =
		    blob.blob(HEX, "0784fa652e733cb699f250b0df2c4b41");
		new i15 =
		    blob.blob(HEX, "106519760fb3ef97e1ccea073b27122d");
		new c15 =
		    blob.blob(HEX, "56a8e0c3ee3315f913693c0ca781e917");
		new aes15 = gcrypt.symmetric(AES, CBC, PKCS7, key=k15.get());

		new p16 =
		    blob.blob(HEX, "c9a44f6f75e98ddbca7332167f5c45e3");
		new k16 =
		    blob.blob(HEX, "04952c3fcf497a4d449c41e8730c5d9a");
		new i16 =
		    blob.blob(HEX, "53549bf7d5553b727458c1abaf0ba167");
		new c16 = blob.blob(HEX,
	    "7fa290322ca7a1a04b61a1147ff20fe66fde58510a1d0289d11c0ddf6f4decfd");
		new aes16 = gcrypt.symmetric(AES, CBC, PKCS7, key=k16.get());

		new p32 = blob.blob(HEX,
	    "1ba93ee6f83752df47909585b3f28e56693f89e169d3093eee85175ea3a46cd3");
		new k32 =
		    blob.blob(HEX, "2ae7081caebe54909820620a44a60a0f");
		new i32 =
		    blob.blob(HEX, "fc5e783fbe7be12f58b1f025d82ada50");
		new c32 = blob.blob(HEX,
"7944957a99e473e2c07eb496a83ec4e55db2fb44ebdd42bb611e0def29b23a73ac37eb0f4f5d86f090f3ddce3980425a");
		new aes32 = gcrypt.symmetric(AES, CBC, PKCS7, key=k32.get());

		new p33 = blob.blob(HEX,
	  "0397f4f6820b1f9386f14403be5ac16e50213bd473b4874b9bcbf5f318ee686b1d");
		new k33 =
		    blob.blob(HEX, "898be9cc5004ed0fa6e117c9a3099d31");
		new i33 =
		    blob.blob(HEX, "9dea7621945988f96491083849b068df");
		new c33 = blob.blob(HEX,
"e232cd6ef50047801ee681ec30f61d53cfd6b0bca02fd03c1b234baa10ea82ac9dab8b960926433a19ce6dea08677e34");
		new aes33 = gcrypt.symmetric(AES, CBC, PKCS7, key=k33.get());

		# The string is: {"encrypt" : "foo"}
		new p_j = blob.blob(encoded="{"+{""encrypt" : "foo""}+"}");
		new k_j =
		    blob.blob(HEX, "dc28c4a23ec612dc63a23d32a5ecdaab");
		new i_j =
		    blob.blob(HEX, "00000000000000000000000000000000");
		new c_j = blob.blob(HEX,
	    "c58c18a02297685e11148e51cbde72e644262e6bc875a3270f207f9e3936c1dd");
		new aes_j = gcrypt.symmetric(AES, CBC, PKCS7, key=k_j.get());
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.c6 =
		    blob.encode(HEX, LOWER, aes6.encrypt(p6.get(), iv=i6.get()));
		set resp.http.p6 =
		    blob.encode(HEX, LOWER, aes6.decrypt(c6.get(), iv=i6.get()));
		set resp.http.c7 =
		    blob.encode(HEX, LOWER, aes7.encrypt(p7.get(), iv=i7.get()));
		set resp.http.p7 =
		    blob.encode(HEX, LOWER, aes7.decrypt(c7.get(), iv=i7.get()));
		set resp.http.c8 =
		    blob.encode(HEX, LOWER, aes8.encrypt(p8.get(), iv=i8.get()));
		set resp.http.p8 =
		    blob.encode(HEX, LOWER, aes8.decrypt(c8.get(), iv=i8.get()));
		set resp.http.c9 =
		    blob.encode(HEX, LOWER, aes9.encrypt(p9.get(), iv=i9.get()));
		set resp.http.p9 =
		    blob.encode(HEX, LOWER, aes9.decrypt(c9.get(), iv=i9.get()));
		set resp.http.c10 = blob.encode(HEX, LOWER,
		                        aes10.encrypt(p10.get(), iv=i10.get()));
		set resp.http.p10 = blob.encode(HEX, LOWER,
					aes10.decrypt(c10.get(), iv=i10.get()));
		set resp.http.c11 = blob.encode(HEX, LOWER,
		                        aes11.encrypt(p11.get(), iv=i11.get()));
		set resp.http.p11 = blob.encode(HEX, LOWER,
					aes11.decrypt(c11.get(), iv=i11.get()));
		set resp.http.c12 = blob.encode(HEX, LOWER,
		                        aes12.encrypt(p12.get(), iv=i12.get()));
		set resp.http.p12 = blob.encode(HEX, LOWER,
					aes12.decrypt(c12.get(), iv=i12.get()));
		set resp.http.c13 = blob.encode(HEX, LOWER,
		                        aes13.encrypt(p13.get(), iv=i13.get()));
		set resp.http.p13 = blob.encode(HEX, LOWER,
					aes13.decrypt(c13.get(), iv=i13.get()));
		set resp.http.c14 = blob.encode(HEX, LOWER,
		                        aes14.encrypt(p14.get(), iv=i14.get()));
		set resp.http.p14 = blob.encode(HEX, LOWER,
					aes14.decrypt(c14.get(), iv=i14.get()));
		set resp.http.c15 = blob.encode(HEX, LOWER,
		                        aes15.encrypt(p15.get(), iv=i15.get()));
		set resp.http.p15 = blob.encode(HEX, LOWER,
					aes15.decrypt(c15.get(), iv=i15.get()));
		set resp.http.c16 = blob.encode(HEX, LOWER,
		                        aes16.encrypt(p16.get(), iv=i16.get()));
		set resp.http.p16 = blob.encode(HEX, LOWER,
					aes16.decrypt(c16.get(), iv=i16.get()));
		set resp.http.c32 = blob.encode(HEX, LOWER,
		                        aes32.encrypt(p32.get(), iv=i32.get()));
		set resp.http.p32 = blob.encode(HEX, LOWER,
					aes32.decrypt(c32.get(), iv=i32.get()));
		set resp.http.c33 = blob.encode(HEX, LOWER,
		                        aes33.encrypt(p33.get(), iv=i33.get()));
		set resp.http.p33 = blob.encode(HEX, LOWER,
					aes33.decrypt(c33.get(), iv=i33.get()));
		set resp.http.c_j = blob.encode(HEX, LOWER,
		                        aes_j.encrypt(p_j.get(), iv=i_j.get()));
		set resp.http.p_j = blob.encode(IDENTITY, DEFAULT,
					aes_j.decrypt(c_j.get(), iv=i_j.get()));
		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.c6 == "e8a846fd9718507371604504d4ca1ac7"
	expect resp.http.p6 == "f6cee5ff28fd"
	expect resp.http.c7 == "009e935f3fe4d57b57fc3127a8873d8c"
	expect resp.http.p7 == "76cdfdf52a9753"
	expect resp.http.c8 == "b3d8df2c3147b0752a7e6bbbcc9d5758"
	expect resp.http.p8 == "b103c928531d8875"
	expect resp.http.c9 == "c11a034ed324aeae9cd5857ae4cd776f"
	expect resp.http.p9 == "590b10224087872724"
	expect resp.http.c10 == "b61ff0a956b420347daa25bb76964b51"
	expect resp.http.p10 == "ccecfa22708b6d06439c"
	expect resp.http.c11 == "3ee8bdb21b00e0103ccbf9afb9b5bd9a"
	expect resp.http.p11 == "8ff539940bae985f2f88f3"
	expect resp.http.c12 == "3e087f92a998ad531e0ff8e996098382"
	expect resp.http.p12 == "4c84974b5b2109d5bc90e1f0"
	expect resp.http.c13 == "a4bfd6586344bcdef94f09d871ca8a16"
	expect resp.http.p13 == "13eb26baf2b688574cadac6dba"
	expect resp.http.c14 == "2b166eae7a2edfea7a482e5f7377069e"
	expect resp.http.p14 == "5fcb46a197ddf80a40f94dc21531"
	expect resp.http.c15 == "56a8e0c3ee3315f913693c0ca781e917"
	expect resp.http.p15 == "6842455a2992c2e5193056a5524075"
	expect resp.http.c16 == "7fa290322ca7a1a04b61a1147ff20fe66fde58510a1d0289d11c0ddf6f4decfd"
	expect resp.http.p16 == "c9a44f6f75e98ddbca7332167f5c45e3"
	expect resp.http.c32 == "7944957a99e473e2c07eb496a83ec4e55db2fb44ebdd42bb611e0def29b23a73ac37eb0f4f5d86f090f3ddce3980425a"
	expect resp.http.p32 == "1ba93ee6f83752df47909585b3f28e56693f89e169d3093eee85175ea3a46cd3"
	expect resp.http.c33 == "e232cd6ef50047801ee681ec30f61d53cfd6b0bca02fd03c1b234baa10ea82ac9dab8b960926433a19ce6dea08677e34"
	expect resp.http.p33 == "0397f4f6820b1f9386f14403be5ac16e50213bd473b4874b9bcbf5f318ee686b1d"
	expect resp.http.c_j == "c58c18a02297685e11148e51cbde72e644262e6bc875a3270f207f9e3936c1dd"
	expect resp.http.p_j == {{"encrypt" : "foo"}}
} -run

# Use random() to create an IV and a counter.
varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k1 = blob.blob(HEX, "000102030405060708090a0b0c0d0e0f");
		new ctr = gcrypt.symmetric(AES, CTR, key=k1.get());
		new cbc = gcrypt.symmetric(AES, CBC, key=k1.get(), cbc_cts=true);
		new p1 = blob.blob(HEX, "000102030405060708090a0b0c0d0e0f");
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.ctr-ciphertext
		    = blob.encode(HEX, LOWER,
		                      ctr.encrypt(p1.get(),
		                                 ctr=gcrypt.random(NONCE, 16B)));
		set resp.http.ctr-ctr
		    = blob.encode(HEX, LOWER, gcrypt.random());
		set resp.http.ctr-plaintext
		    = blob.encode(HEX, LOWER,
		     ctr.decrypt(blob.decode(HEX, encoded=resp.http.ctr-ciphertext),
		                 ctr=blob.decode(HEX, encoded=resp.http.ctr-ctr)));
		set resp.http.cbc-ciphertext
		    = blob.encode(HEX, LOWER,
		                      cbc.encrypt(p1.get(),
		                                iv=gcrypt.random(STRONG, 16B)));
		set resp.http.cbc-iv
		    = blob.encode(HEX, LOWER, gcrypt.random());
		set resp.http.cbc-plaintext
		    = blob.encode(HEX, LOWER,
		     cbc.decrypt(blob.decode(HEX, encoded=resp.http.cbc-ciphertext),
		                 iv=blob.decode(HEX, encoded=resp.http.cbc-iv)));
		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.ctr-ciphertext ~ "^[[:xdigit:]]{32}$"
	expect resp.http.ctr-ctr ~ "^[[:xdigit:]]{32}$"
	expect resp.http.ctr-plaintext == "000102030405060708090a0b0c0d0e0f"
	expect resp.http.cbc-ciphertext ~ "^[[:xdigit:]]{32}$"
	expect resp.http.cbc-iv ~ "^[[:xdigit:]]{32}$"
	expect resp.http.cbc-plaintext == "000102030405060708090a0b0c0d0e0f"
} -run

# Repeat the previous test using secure memory
varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k1 = blob.blob(HEX, "000102030405060708090a0b0c0d0e0f");
		new ctr = gcrypt.symmetric(AES, CTR, key=k1.get(), secure=true);
		new cbc = gcrypt.symmetric(AES, CBC, key=k1.get(),
		                           cbc_cts=true, secure=true);
		new p1 = blob.blob(HEX, "000102030405060708090a0b0c0d0e0f");
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.ctr-ciphertext
		    = blob.encode(HEX, LOWER,
		                      ctr.encrypt(p1.get(),
		                                 ctr=gcrypt.random(NONCE, 16B)));
		set resp.http.ctr-ctr
		    = blob.encode(HEX, LOWER, gcrypt.random());
		set resp.http.ctr-plaintext
		    = blob.encode(HEX, LOWER,
		     ctr.decrypt(blob.decode(HEX, encoded=resp.http.ctr-ciphertext),
		                 ctr=blob.decode(HEX, encoded=resp.http.ctr-ctr)));
		set resp.http.cbc-ciphertext
		    = blob.encode(HEX, LOWER,
		                      cbc.encrypt(p1.get(),
		                                iv=gcrypt.random(STRONG, 16B)));
		set resp.http.cbc-iv
		    = blob.encode(HEX, LOWER, gcrypt.random());
		set resp.http.cbc-plaintext
		    = blob.encode(HEX, LOWER,
		     cbc.decrypt(blob.decode(HEX, encoded=resp.http.cbc-ciphertext),
		                 iv=blob.decode(HEX, encoded=resp.http.cbc-iv)));
		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.ctr-ciphertext ~ "^[[:xdigit:]]{32}$"
	expect resp.http.ctr-ctr ~ "^[[:xdigit:]]{32}$"
	expect resp.http.ctr-plaintext == "000102030405060708090a0b0c0d0e0f"
	expect resp.http.cbc-ciphertext ~ "^[[:xdigit:]]{32}$"
	expect resp.http.cbc-iv ~ "^[[:xdigit:]]{32}$"
	expect resp.http.cbc-plaintext == "000102030405060708090a0b0c0d0e0f"
} -run
