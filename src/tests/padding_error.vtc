# looks like -*- vcl -*-

varnishtest "padding"

varnish v1 -vcl {
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		gcrypt.init(FINISH);
	}
} -start

# Padding errors
#
# XXX these tests lead to "vmod blob error: cannot encode, out of space"
# even for workspace_client=512k

varnish v1 -vcl {
	import blob;
	import gcrypt from "${vmod_topbuild}/src/.libs/libvmod_gcrypt.so";
	backend b { .host = "${bad_ip}"; }

	sub vcl_init {
		new k1 = blob.blob(HEX, "000102030405060708090a0b0c0d0e0f");
		new none = gcrypt.symmetric(AES, ECB, NONE, key=k1.get());
		new pkcs7 = gcrypt.symmetric(AES, ECB, PKCS7, key=k1.get());
		new iso7816 = gcrypt.symmetric(AES, ECB, ISO7816, key=k1.get());
		new x923 = gcrypt.symmetric(AES, ECB, X923, key=k1.get());

		new foo_pkcs7_1
		    = blob.blob(HEX, "666F6F0E0D0D0D0D0D0D0D0D0D0D0D0D");
		new foo_pkcs7_2
		    = blob.blob(HEX, "666F6F0D0D0D0D0D0D0D0D0D0D0D0D11");
		new foo_iso7816_1
		    = blob.blob(HEX, "666F6F80000000000001000000000000");
		new foo_iso7816_2
		    = blob.blob(HEX, "666F6F00000000000000000000000000");
		new foo_x923_1
		    = blob.blob(HEX, "666F6F0000000001000000000000000D");
		new foo_x923_2
		    = blob.blob(HEX, "666F6F00000000000000000000000011");

		new empty_pkcs7_1
		    = blob.blob(HEX, "0F101010101010101010101010101010");
		new empty_pkcs7_2
		    = blob.blob(HEX, "10101010101010101010101010101011");
		new empty_iso7816_1
		    = blob.blob(HEX, "00000000000000000000000000000000");
		new empty_iso7816_2
		    = blob.blob(HEX, "80010000000000000000000000000000");
		new empty_x923_1
		    = blob.blob(HEX, "01000000000000000000000000000010");
		new empty_x923_2
		    = blob.blob(HEX, "00000000000000000000000000000011");
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.foo-pkcs7-cipher-1
		    = blob.encode(BASE64, DEFAULT, none.encrypt(foo_pkcs7_1.get()));
		set resp.http.foo-pkcs7-plain-1
		    = blob.encode(blob=pkcs7.decrypt(
		        blob.decode(BASE64, encoded=resp.http.foo-pkcs7-cipher-1)));
		set resp.http.foo-pkcs7-cipher-2
		    = blob.encode(BASE64, DEFAULT, none.encrypt(foo_pkcs7_2.get()));
		set resp.http.foo-pkcs7-plain-2
		    = blob.encode(blob=pkcs7.decrypt(
		        blob.decode(BASE64, encoded=resp.http.foo-pkcs7-cipher-2)));
		set resp.http.foo-iso7816-cipher-1
		   = blob.encode(BASE64, DEFAULT, none.encrypt(foo_iso7816_1.get()));
		set resp.http.foo-iso7816-plain-1
		    = blob.encode(blob=iso7816.decrypt(
		      blob.decode(BASE64, encoded=resp.http.foo-iso7816-cipher-1)));
		set resp.http.foo-iso7816-cipher-2
		   = blob.encode(BASE64, DEFAULT, none.encrypt(foo_iso7816_2.get()));
		set resp.http.foo-iso7816-plain-2
		    = blob.encode(blob=iso7816.decrypt(
		      blob.decode(BASE64, encoded=resp.http.foo-iso7816-cipher-2)));
		set resp.http.foo-x923-cipher-1
		    = blob.encode(BASE64, DEFAULT, none.encrypt(foo_x923_1.get()));
		set resp.http.foo-x923-plain-1
		    = blob.encode(blob=x923.decrypt(
		        blob.decode(BASE64, encoded=resp.http.foo-x923-cipher-1)));
		set resp.http.foo-x923-cipher-2
		    = blob.encode(BASE64, DEFAULT, none.encrypt(foo_x923_1.get()));
		set resp.http.foo-x923-plain-2
		    = blob.encode(blob=x923.decrypt(
		        blob.decode(BASE64, encoded=resp.http.foo-x923-cipher-2)));

		set resp.http.empty-pkcs7-cipher-1
		   = blob.encode(BASE64, DEFAULT, none.encrypt(empty_pkcs7_1.get()));
		set resp.http.empty-pkcs7-plain-1
		    = blob.encode(blob=pkcs7.decrypt(
		      blob.decode(BASE64, encoded=resp.http.empty-pkcs7-cipher-1)));
		set resp.http.empty-pkcs7-cipher-2
		   = blob.encode(BASE64, DEFAULT, none.encrypt(empty_pkcs7_2.get()));
		set resp.http.empty-pkcs7-plain-2
		    = blob.encode(blob=pkcs7.decrypt(
		      blob.decode(BASE64, encoded=resp.http.empty-pkcs7-cipher-2)));
		set resp.http.empty-iso7816-cipher-1
		 = blob.encode(BASE64, DEFAULT, none.encrypt(empty_iso7816_1.get()));
		set resp.http.empty-iso7816-plain-1
		    = blob.encode(blob=iso7816.decrypt(
		    blob.decode(BASE64, encoded=resp.http.empty-iso7816-cipher-1)));
		set resp.http.empty-iso7816-cipher-2
		 = blob.encode(BASE64, DEFAULT, none.encrypt(empty_iso7816_2.get()));
		set resp.http.empty-iso7816-plain-2
		    = blob.encode(blob=iso7816.decrypt(
		    blob.decode(BASE64, encoded=resp.http.empty-iso7816-cipher-2)));
		set resp.http.empty-x923-cipher-1
		    = blob.encode(BASE64, DEFAULT, none.encrypt(empty_x923_1.get()));
		set resp.http.empty-x923-plain-1
		    = blob.encode(blob=x923.decrypt(
		       blob.decode(BASE64, encoded=resp.http.empty-x923-cipher-1)));
		set resp.http.empty-x923-cipher-2
		    = blob.encode(BASE64, DEFAULT, none.encrypt(empty_x923_1.get()));
		set resp.http.empty-x923-plain-2
		    = blob.encode(blob=x923.decrypt(
		       blob.decode(BASE64, encoded=resp.http.empty-x923-cipher-2)));
		return(deliver);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.foo-pkcs7-cipher-1 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-pkcs7-plain-1 == ""
	expect resp.http.foo-pkcs7-cipher-2 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-pkcs7-plain-2 == ""
	expect resp.http.foo-iso7816-cipher-1 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-iso7816-plain-1 == ""
	expect resp.http.foo-iso7816-cipher-2 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-iso7816-plain-2 == ""
	expect resp.http.foo-x923-cipher-1 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-x923-plain-1 == ""
	expect resp.http.foo-x923-cipher-2 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.foo-x923-plain-2 == ""
	expect resp.http.empty-pkcs7-cipher-1 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-pkcs7-plain-1 == ""
	expect resp.http.empty-pkcs7-cipher-2 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-pkcs7-plain-2 == ""
	expect resp.http.empty-iso7816-cipher-1 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-iso7816-plain-1 == ""
	expect resp.http.empty-iso7816-cipher-2 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-iso7816-plain-2 == ""
	expect resp.http.empty-x923-cipher-1 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-x923-plain-1 == ""
	expect resp.http.empty-x923-cipher-2 ~ "^[A-Za-z0-9+/]+=*$"
	expect resp.http.empty-x923-plain-2 == ""
} -run

logexpect l1 -v v1 -d 1 -q "VCL_Error" {
	expect 0 * Begin req
	expect * = VCL_Error "^vmod gcrypt error: in pkcs7.decrypt..: incorrect padding$"
	expect * = VCL_Error "^vmod gcrypt error: in pkcs7.decrypt..: incorrect padding$"
	expect * = VCL_Error "^vmod gcrypt error: in iso7816.decrypt..: incorrect padding$"
	expect * = VCL_Error "^vmod gcrypt error: in iso7816.decrypt..: incorrect padding$"
	expect * = VCL_Error "^vmod gcrypt error: in x923.decrypt..: incorrect padding$"
	expect * = VCL_Error "^vmod gcrypt error: in x923.decrypt..: incorrect padding$"
	expect * = VCL_Error "^vmod gcrypt error: in pkcs7.decrypt..: incorrect padding$"
	expect * = VCL_Error "^vmod gcrypt error: in pkcs7.decrypt..: incorrect padding$"
	expect * = VCL_Error "^vmod gcrypt error: in iso7816.decrypt..: incorrect padding$"
	expect * = VCL_Error "^vmod gcrypt error: in iso7816.decrypt..: incorrect padding$"
	expect * = VCL_Error "^vmod gcrypt error: in x923.decrypt..: incorrect padding$"
	expect * = VCL_Error "^vmod gcrypt error: in x923.decrypt..: incorrect padding$"
	expect * = End
} -run

