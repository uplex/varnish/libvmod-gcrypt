/*-
 * Copyright 2017 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

typedef void* pad_f(struct ws *ws, const void *src, size_t len, size_t blocklen,
		    size_t *newlen);

static inline void *
pad(struct ws *ws, const void *src, size_t len, size_t blocklen, size_t *newlen,
    size_t *padlen)
{
	void *dst;

	assert(blocklen > 0);

	*newlen = ((len + blocklen) / blocklen) * blocklen;
	*padlen = *newlen - len;
	if ((dst = WS_Alloc(ws, *newlen)) == NULL)
		return NULL;
	memcpy(dst, src, len);
	return dst;
}

static void *
pad_pkcs7(struct ws *ws, const void *src, size_t len, size_t blocklen,
    size_t *newlen)
{
	size_t padlen;
	unsigned char *dst;

	dst = pad(ws, src, len, blocklen, newlen, &padlen);
	if (dst == NULL)
		return NULL;
	for (unsigned i = len; i < *newlen; i++)
		dst[i] = padlen;
	return dst;
}

static void *
pad_iso7816(struct ws *ws, const void *src, size_t len, size_t blocklen,
	    size_t *newlen)
{
	size_t padlen;
	unsigned char *dst;

	dst = pad(ws, src, len, blocklen, newlen, &padlen);
	if (dst == NULL)
		return NULL;
	dst[len] = 0x80;
	for (unsigned i = len + 1; i < *newlen; i++)
		dst[i] = 0;
	return dst;
}

static void *
pad_x923(struct ws *ws, const void *src, size_t len, size_t blocklen,
    size_t *newlen)
{
	size_t padlen;
	unsigned char *dst;

	dst = pad(ws, src, len, blocklen, newlen, &padlen);
	if (dst == NULL)
		return NULL;
	dst[*newlen - 1] = padlen;
	for (unsigned i = len; i < *newlen - 1; i++)
		dst[i] = 0;
	return dst;
}

static pad_f * const padf[] = {
	[PKCS7]		= pad_pkcs7,
	[ISO7816]	= pad_iso7816,
	[X923]		= pad_x923,
};

typedef ssize_t unpadlen_f(void *plaintext, int cipherlen, size_t blocklen);

static ssize_t
unpadlen_pkcs7(void *plaintext, int cipherlen, size_t blocklen)
{
	uint8_t padlen = ((uint8_t *)plaintext)[cipherlen-1];

	if (padlen > blocklen || padlen > cipherlen)
		return -1;
	for (int i = cipherlen - 2; i >= cipherlen - padlen; i--)
		if (padlen != ((uint8_t *)plaintext)[i])
			return -1;
	return cipherlen - padlen;
}

static ssize_t
unpadlen_iso7816(void *plaintext, int cipherlen, size_t blocklen)
{
	(void) blocklen;
	int i = cipherlen - 1;
	while (i >= 0 && ((uint8_t *)plaintext)[i] != 0x80)
		if (0 != ((uint8_t *)plaintext)[i--])
			return -1;
	return i;
}

static ssize_t
unpadlen_x923(void *plaintext, int cipherlen, size_t blocklen)
{
	uint8_t padlen = ((uint8_t *)plaintext)[cipherlen-1];

	if (padlen > blocklen || padlen > cipherlen)
		return -1;
	for (int i = cipherlen - 2; i >= cipherlen - padlen; i--)
		if (0 != ((uint8_t *)plaintext)[i])
			return -1;
	return cipherlen - padlen;
}

static unpadlen_f * const unpadlenf[] = {
	[PKCS7]		= unpadlen_pkcs7,
	[ISO7816]	= unpadlen_iso7816,
	[X923]		= unpadlen_x923,
};
